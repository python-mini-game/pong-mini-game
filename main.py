from turtle import Screen
from paddle import Paddle
from ball import Ball
import time
from scoreboard import Scoreboard

screen = Screen()
screen.setup(width=800, height=600)
screen.bgcolor("black")
screen.title("Pong")
screen.tracer(0)



r_paddle = Paddle((350,0))
l_paddle = Paddle((-350,0))
ball = Ball()
scoreboard = Scoreboard()


screen.listen()
screen.onkeypress(r_paddle.go_up, "Up")
screen.onkeypress(r_paddle.go_down, "Down")
screen.onkeypress(l_paddle.go_up,"w")
screen.onkeypress(l_paddle.go_down, "s")

game_is_on = True
while game_is_on:
    time.sleep(ball.move_speed)
    screen.update()
    ball.move()
    
    
    #detect wall collisions with the ball
    if ball.ycor() > 280 or ball.ycor() < -280:
        ball.bounce_y()
    
    
    #Detect collision with r_paddle
    if ball.distance(r_paddle) < 70 and ball.xcor() > 320 or ball.distance(l_paddle) < 70 and ball.xcor() < -320:
        ball.bounce_x()
    #Give point to the left    
    elif  ball.xcor() > 390 :
        scoreboard.l_point()
        ball.move_ball_back()
    #Give point to the right    
    elif ball.xcor() < -390:
        scoreboard.r_point()
        ball.move_ball_back()



screen.exitonclick()
